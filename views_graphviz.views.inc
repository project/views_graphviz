<?php

/**
 * @file
 * Defines the View Style Plugins for Views Graphviz module.
 */

/**
 * Implements hook_views_plugins().
 */
function views_graphviz_views_plugins() {
  return array(
    'style' => array(
      'graphviz' => array(
        'title' => t('Graphviz'),
        'help' => t('Display the results as a GraphViz Graph.'),
        'handler' => 'views_graphviz_plugin_style_graphviz',
        'theme' => 'views_graphviz',
        'uses options' => TRUE,
        'uses row plugin' => TRUE,
        'type' => 'normal',
        'parent' => 'table',
      ),
    ),
    'row' => array(
      'graphviz_node' => array(
        'title' => t('Raw graphviz node'),
        'help' => t('(Formats text as labels and links as [URL=] snippets.'),
        'handler' => 'views_plugin_row_graphviz_node',
        'theme' => 'views_view_row_graphviz_node',
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
