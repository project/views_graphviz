<?php

/**
 * @file
 * Contains the table style plugin.
 */

/**
 * Style plugin to render each item in a graphviz dot file
 *
 * @ingroup views_style_plugins
 */
class views_graphviz_plugin_style_graphviz extends views_plugin_style_table {

  // Set default options
  function option_definition() {
    $options = parent::option_definition();

    $options['mode'] = array('default' => 'dot');
    $options['direction'] = array('default' => '');

    return $options;
  }

  // Render the given style.
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $form['mode'] = array(
      '#type' => 'select',
      '#title' => t('Graphviz Rendering mode'),
      '#options' => array(
          'dot' => 'dot',
          'neato' => 'neato',
        ),
      '#default_value' => $this->options['mode'],
    );
    $form['direction'] = array(
      '#type' => 'select',
      '#title' => 'Dot rendering direction',
      '#options' => array(''=> t('Default'), 'LR'=>t('Left-Right')),
      '#default_value' => $this->options['direction'],
    );
   
  }
  
  function pre_render($result) {
    // iterate through result set, as follows:
    // 1. Find each nid field, and create as column.
    // 2. Use unique nid in each column as key.
    // For #1, we just need first result
    $all_fields = $this->view->field;
    $fields = array();
    $f = 0; //field index
    foreach ($all_fields as $key=>$val) {
      if (!$val->options['exclude']) {
        $fields[] = array(
          'f'=>'f'.++$f,
          'nid'=>$val->aliases['nid'],
          'field'=>$val->field_alias,
          'fieldlbl' => $val->options['label'],
        );
      }
    }
    $this->graphviz_fields = $fields;
    $this->objects = array();
    foreach ($result as $row) {
      foreach ($fields as $key=>$field) {
        $nid = $field['nid'];
        $fieldname = $field['field'];
//        if (!empty($row->$fieldname)) {
            $this->objects[$field['f']][$row->$nid] = $row->$fieldname;
//        }
      }
    }
  }
  
  function render() {
    require_once(_graphviz_create_filepath(variable_get('graphviz_filter_pear_path', ''), 'Image/GraphViz.php'));
    $G = new Image_Graphviz(TRUE);
    
    $G->addAttributes(array(
      'rankdir' => 'LR',
    //  'size' => '12,14',
    ));
    // result set is on $this->view->result.
    // First output all the nodes:
    $result = $this->view->result;
    foreach ($this->objects as $f=>$objects){
      foreach ($objects as $nid=>$label) {
        $label = wordwrap($label,18);
        $G->addNode('node_'.$f.'_'.$nid, array('label'=>$label,
          'URL'=> url('node/'.$nid),
          'shape'=>'box',
         // 'fixedsize' => true,
           'width' => 2.2,
        ));
        //$label = htmlspecialchars($label);
        //$output .= 'node_'. $nid.' [label="'.$label.'"]'.";\n";
      }
    }
    
    // next iterate through rows to build links, but don't duplicate...
    //$connector = variable_get('views_graphviz_link', '->');
    $linked = array();
    foreach ($result as $row) {
      $fields = $this->graphviz_fields;
      $field1 = array_shift($fields);
      $field1_nid = $field1['nid'];
      $field1_label = $field1['field'];
      $f1 = $field1['f'];
      while (count($fields)) {
        $field2 = array_shift($fields);

        $field2_nid = $field2['nid'];
        $field2_label = $field2['field'];
        $f2 = $field2['f'];
  //      if (empty($row->$field1_label) || empty($row->$field2_label)) {
          // do nothing -- need to swap fields...
  //      } else {
          $link1 = 'node_'.$f1.'_' .$row->$field1_nid;
          $link2 = 'node_'.$f2.'_' .$row->$field2_nid;
          $link = $link1 .' '. $connector.' '. $link2.";\n";

          if (!in_array($link, $linked)) {
            $G->addEdge(array($link1=>$link2));
            $linked[] = $link;
          //$output .= $link;
          }
    //    }
        $field1 = $field2;
        $f1 = $f2;
        $field1_nid = $field2_nid;
        $field1_label = $field2_label;
      }
    }
        // output header labels
    $lastfield = false;
    if (variable_get('views_graphviz_labels',true)) {
    //  $G->addSubgraph('labels','labels',array());
      foreach ($this->graphviz_fields as $field) {
        $G->addNode('lbl_'.$field['f'], array('label'=>$field['fieldlbl'],'shape'=>'plaintext'));
       // $G->addNode('lbl_'.$field['f'], null, $field['f']);
        //$G->addSubgraph($field['f'],$field['f'],array('rank'=>'same'));
        if ($lastfield) {
          $G->addEdge(array($lastfield=>'lbl_'.$field['f']));
        }
        $lastfield = 'lbl_'.$field['f'];
      }
        
    }


    return graphviz_filter_render($G);
  }
  
}
